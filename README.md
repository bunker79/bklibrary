# BKLibrary

[![CI Status](https://img.shields.io/travis/rogerio-shimizu-bunker79/BKLibrary.svg?style=flat)](https://travis-ci.org/rogerio-shimizu-bunker79/BKLibrary)
[![Version](https://img.shields.io/cocoapods/v/BKLibrary.svg?style=flat)](https://cocoapods.org/pods/BKLibrary)
[![License](https://img.shields.io/cocoapods/l/BKLibrary.svg?style=flat)](https://cocoapods.org/pods/BKLibrary)
[![Platform](https://img.shields.io/cocoapods/p/BKLibrary.svg?style=flat)](https://cocoapods.org/pods/BKLibrary)



## Requirements

This library requires iOS 10

## Installation

BKLibrary is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'BKLibrary'
```

It will install all UI classes. If you want extensions or other utils, use
```ruby
pod 'BKLibrary/Extension'
pod 'BKLibrary/Util'
```




## Author

Rogerio Shimizu, roja@bunker79.com
Andre Assis, andre@bunker79.com

## License

BKLibrary is available under the MIT license. See the LICENSE file for more info.
