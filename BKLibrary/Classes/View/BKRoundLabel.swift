//
//  RoundLabel.swift
//  InterfaceKit
//
//  Created by Rogerio Shimizu on 03/01/18.
//  Copyright © 2018 Topsports Ventures S.A. All rights reserved.
//

import UIKit

@IBDesignable
public class BKRoundLabel: UILabel {

    @IBInspectable open var cornerRadius:CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }

}
