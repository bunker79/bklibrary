//
//  BKSwitch.swift
//  BKLibrary
//
//  Created by Rogerio Shimizu on 04/09/18.
//

import UIKit

@IBDesignable
open class BKTextSwitch: UIControl {
    
    @IBInspectable
    open var titleCollection: String = "" {
        didSet {
            titles = titleCollection.split(separator: "|").compactMap({ String($0)})
        }
    }
    
    @IBInspectable
    open var corner: CGFloat = -1 {
        didSet {
            if corner >= 0 {
                self.layer.cornerRadius = corner
                titleMaskView.layer.cornerRadius = corner
                selectedBackgroundView.layer.cornerRadius = corner
            } else {
                self.layer.cornerRadius = bounds.height/2.0
                titleMaskView.layer.cornerRadius = titleMaskView.bounds.height/2.0
                selectedBackgroundView.layer.cornerRadius = selectedBackgroundView.bounds.height/2.0
            }
        }
    }
    
    open var titles: [String] {
        set {
            (titleLabels + selectedTitleLabels).forEach { $0.removeFromSuperview() }
            titleLabels = newValue.map { title in
                let label = UILabel()
                label.text = title
                label.textColor = titleColor
                label.font = titleFont
                label.textAlignment = .center
                label.lineBreakMode = .byTruncatingTail
                titleLabelsContentView.addSubview(label)
                return label
            }
            selectedTitleLabels = newValue.map { title in
                let label = UILabel()
                label.text = title
                label.textColor = selectedTitleColor
                label.font = titleFont
                label.textAlignment = .center
                label.lineBreakMode = .byTruncatingTail
                selectedTitleLabelsContentView.addSubview(label)
                return label
            }
            
            updateInteface()
        }
        get { return titleLabels.map { $0.text! } }
        
    }
    
    @IBInspectable
    open var selectedIndex: Int = 0 {
        didSet { setNeedsLayout() }
    }
    
    @IBInspectable
    open var selectedBackgroundInset: CGFloat = 2.0 {
        didSet { setNeedsLayout() }
    }
    
    @IBInspectable //just added this cuz @objc was making the property not inspectable...
    open var selectedBackgroundColor: UIColor! {
        didSet {
            _selectedBackgroundColor = selectedBackgroundColor
        }
    }
    
    @objc private var _selectedBackgroundColor: UIColor! {
        set { selectedBackgroundView.backgroundColor = newValue }
        get { return selectedBackgroundView.backgroundColor }
    }
    
    @IBInspectable
    open var titleColor: UIColor! {
        didSet {
            titleLabels.forEach { $0.textColor = titleColor }
        }
    }
    
    @IBInspectable
    open var selectedTitleColor: UIColor! {
        didSet {
            selectedTitleLabels.forEach { $0.textColor = selectedTitleColor }
        }
    }
    
    open var titleFont: UIFont! {
        didSet {
            (titleLabels + selectedTitleLabels).forEach { $0.font = titleFont }
        }
    }
    
    @IBInspectable
    open var titleFontFamily: String = "HelveticaNeue" {
        didSet {
            self.titleFont = UIFont(name: self.titleFontFamily, size: self.titleFontSize)
        }
    }
    
    @IBInspectable
    open var titleFontSize: CGFloat = 18.0 {
        didSet {
            self.titleFont = UIFont(name: self.titleFontFamily, size: self.titleFontSize)
        }
    }
    
    open var animationDuration: TimeInterval = 0.3
    open var animationSpringDamping: CGFloat = 0.75
    open var animationInitialSpringVelocity: CGFloat = 0.0
    
    fileprivate var titleLabelsContentView = UIView()
    fileprivate var titleLabels = [UILabel]()
    
    fileprivate var selectedTitleLabelsContentView = UIView()
    fileprivate var selectedTitleLabels = [UILabel]()
    
    @objc fileprivate(set) var selectedBackgroundView = UIView()
    
    fileprivate var titleMaskView: UIView = UIView()
    
    fileprivate var tapGesture: UITapGestureRecognizer!
    fileprivate var panGesture: UIPanGestureRecognizer!
    fileprivate var swipeLGesture: UISwipeGestureRecognizer!
    fileprivate var swipeRGesture: UISwipeGestureRecognizer!
    
    fileprivate var initialSelectedBackgroundViewFrame: CGRect?
    var targetColor: UIColor?
    
    // MARK: - Constructors
    
    public init(titles: [String]) {
        super.init(frame: CGRect.zero)
        
        self.titles = titles
        
        internalInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        internalInit()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        internalInit()
        backgroundColor = .black // don't set background color in finishInit(), otherwise IB settings which are applied in init?(coder:) are overwritten
    }
    
    fileprivate func internalInit() {
        // Setup views
        addSubview(titleLabelsContentView)
        
        //object_setClass(selectedBackgroundView.layer, BKSwitchRoundedLayer.self)
        addSubview(selectedBackgroundView)
        
        addSubview(selectedTitleLabelsContentView)
        
        //object_setClass(titleMaskView.layer, BKSwitchRoundedLayer.self)
        //titleMaskView.layer.cornerRadius = self.layer.cornerRadius
        titleMaskView.backgroundColor = .black
        selectedTitleLabelsContentView.layer.mask = titleMaskView.layer
        
        // Setup defaul colors
        if backgroundColor == nil {
            backgroundColor = .black
        }
        
        selectedBackgroundColor = .white
        titleColor = .white
        selectedTitleColor = .black
        
        // Gestures
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapped))
        addGestureRecognizer(tapGesture)
        
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(pan))
        panGesture.delegate = self
        addGestureRecognizer(panGesture)
        
        swipeLGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipe))
        swipeLGesture.delegate = self
        swipeLGesture.direction = .left
        addGestureRecognizer(swipeLGesture)
        
        swipeRGesture = UISwipeGestureRecognizer(target: self, action: #selector(swipe))
        swipeRGesture.delegate = self
        swipeRGesture.direction = .right
        addGestureRecognizer(swipeRGesture)
        
        
        addObserver(self, forKeyPath: "selectedBackgroundView.frame", options: .new, context: nil)
        //addObserver(self, forKeyPath: "selectedBackgroundView.frame", options: .new, context: nil)
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        self.titleFont = UIFont(name: self.titleFontFamily, size: self.titleFontSize)
    }
    
    // MARK: - Destructor
    
    deinit {
        removeObserver(self, forKeyPath: "selectedBackgroundView.frame")
    }
    
    // MARK: - Observer
    
    @objc override open func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "selectedBackgroundView.frame" {
            titleMaskView.frame = selectedBackgroundView.frame
        }
    }
    
    // MARK: -
    
    @objc func tapped(_ gesture: UITapGestureRecognizer!) {
        if titleLabels.count == 2 {
            //tap will switch between selected items
            if selectedIndex == 0 {
                setSelectedIndex(1, animated: true)
            } else {
                setSelectedIndex(0, animated: true)
            }
            return
        }
        
        let location = gesture.location(in: self)
        let index = Int(location.x / (bounds.width / CGFloat(titleLabels.count)))
        setSelectedIndex(index, animated: true)
    }
    
    @objc func pan(_ gesture: UIPanGestureRecognizer!) {
        if gesture.state == .began {
            initialSelectedBackgroundViewFrame = selectedBackgroundView.frame
        } else if gesture.state == .changed {
            var frame = initialSelectedBackgroundViewFrame!
            frame.origin.x += gesture.translation(in: self).x
            frame.origin.x = max(min(frame.origin.x, bounds.width - selectedBackgroundInset - frame.width), selectedBackgroundInset)
            selectedBackgroundView.frame = frame
        } else if gesture.state == .ended || gesture.state == .failed || gesture.state == .cancelled {
            let index = max(0, min(titleLabels.count - 1, Int(selectedBackgroundView.center.x / (bounds.width / CGFloat(titleLabels.count)))))
            setSelectedIndex(index, animated: true)
        }
    }
    
    @objc func swipe(_ gesture: UISwipeGestureRecognizer!) {
        if gesture.direction == .left {
            if selectedIndex > 0 {
                setSelectedIndex(selectedIndex - 1, animated: true)
            }
        } else if gesture.direction == .right {
            if selectedIndex < titleLabels.count-1 {
                setSelectedIndex(selectedIndex + 1, animated: true)
            }
        }
    }
    
    open func setSelectedIndex(_ selectedIndex: Int, animated: Bool) {
        guard 0..<titleLabels.count ~= selectedIndex else { return }
        
        // Reset switch on half pan gestures
        var catchHalfSwitch = false
        if self.selectedIndex == selectedIndex {
            catchHalfSwitch = true
        }
        
        self.selectedIndex = selectedIndex
        if animated {
            if (!catchHalfSwitch) {
                self.sendActions(for: .valueChanged)
            }
            UIView.animate(withDuration: animationDuration, delay: 0.0, usingSpringWithDamping: animationSpringDamping, initialSpringVelocity: animationInitialSpringVelocity, options: [UIView.AnimationOptions.beginFromCurrentState, UIView.AnimationOptions.curveEaseOut], animations: { () -> Void in
                
                if let targetColor = self.targetColor {
                    self.backgroundColor = targetColor
                }
                
                self.layoutSubviews()
                
            }, completion: nil)
        } else {
            layoutSubviews()
            sendActions(for: .valueChanged)
        }
    }
    
    func updateInteface() {
        let selectedBackgroundWidth: CGFloat
        if titleLabels.count > 0 {
            selectedBackgroundWidth = bounds.width / CGFloat(titleLabels.count) - selectedBackgroundInset * 2.0
        } else {
            selectedBackgroundWidth = bounds.width
        }
        selectedBackgroundView.frame = CGRect(x: selectedBackgroundInset + CGFloat(selectedIndex) * (selectedBackgroundWidth + selectedBackgroundInset * 2.0), y: selectedBackgroundInset, width: selectedBackgroundWidth, height: bounds.height - selectedBackgroundInset * 2.0)
        
        (titleLabelsContentView.frame, selectedTitleLabelsContentView.frame) = (bounds, bounds)
        
        let titleLabelMaxWidth = selectedBackgroundWidth
        let titleLabelMaxHeight = bounds.height - selectedBackgroundInset * 2.0
        
        for (label, selectedLabel) in zip(titleLabels, selectedTitleLabels) {
            let index = titleLabels.firstIndex(of: label)!
            
            var size = label.sizeThatFits(CGSize(width: titleLabelMaxWidth, height: titleLabelMaxHeight))
            size.width = min(size.width, titleLabelMaxWidth)
            
            let x = floor((bounds.width / CGFloat(titleLabels.count)) * CGFloat(index) + (bounds.width / CGFloat(titleLabels.count) - size.width) / 2.0)
            let y = floor((bounds.height - size.height) / 2.0)
            let origin = CGPoint(x: x, y: y)
            
            let frame = CGRect(origin: origin, size: size)
            label.frame = frame
            selectedLabel.frame = frame
        }
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        updateInteface()
    }
    
}

// MARK: - UIGestureRecognizerDelegate
extension BKTextSwitch: UIGestureRecognizerDelegate {
    
    override open func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == panGesture {
            return selectedBackgroundView.frame.contains(gestureRecognizer.location(in: self))
        }
        return super.gestureRecognizerShouldBegin(gestureRecognizer)
    }
    
}
