//
//  BKAlertDialog.swift
//  Components
//
//  Created by Rogerio Shimizu on 5/16/16.
//  Copyright © 2016 BUNKER79. All rights reserved.
//

import Foundation
import UIKit

protocol DialogLayouter {
    func setWidth(width:CGFloat)
}

public enum ButtonOrientation {
    case Horizontal
    case Vertical
}

public enum BKAlertButtonStyle {
    case strong
    case `default`
    case weak
}

public enum BKAlertTextStyle {
    case title
    case subtitle
    case text
}

public protocol BKAlertButtonTheme: class {
    static var font: UIFont {get}
    static var textSize: CGFloat {get}
    static var textColor: UIColor {get}
    static var cornerRadius: CGFloat {get}
    static var borderWidth: CGFloat {get}
    static var borderColor: UIColor {get}
    static var backgroundColor: UIColor {get}
    static var horizontalEdgeInsetsValue: CGFloat {get}
    static var buttonHeight: CGFloat {get}
    static var topPaddingToTitle: CGFloat {get}
    static var topPaddingToItems: CGFloat {get}
}

public protocol BKAlertTextTheme: class {
    static var font: UIFont {get}
    static var textSize: CGFloat {get}
    static var textColor: UIColor {get}
    static var alignment: NSTextAlignment {get}
    static var topPadding: CGFloat {get}
}

public protocol BKAlertDialogTheme: class {
    static func buttonThemeForStyle(_ style: BKAlertButtonStyle) -> BKAlertButtonTheme.Type
    static func textThemeForStyle(_ style: BKAlertTextStyle) -> BKAlertTextTheme.Type
    static var backgroundColor: UIColor {get}
    static var horizontalContainerPaddings: CGFloat {get}
    static var topContainerPadding: CGFloat {get}
    static var bottomContainerPadding: CGFloat {get}
    static var cornerRadius: CGFloat {get}
    static var strongButtonTheme: BKAlertButtonTheme.Type {get}
    static var defaultButtonTheme: BKAlertButtonTheme.Type {get}
    static var weakButtonTheme: BKAlertButtonTheme.Type {get}
    static var titleTheme: BKAlertTextTheme.Type {get}
    static var subtitleTheme: BKAlertTextTheme.Type {get}
    static var textTheme: BKAlertTextTheme.Type {get}
}

class BKDefaultTheme: BKAlertDialogTheme {
    
    class BKDefaultStrongButtonTheme: BKAlertButtonTheme {
        
        static var buttonHeight: CGFloat {
            return 40.0
        }
        
        static var topPaddingToTitle: CGFloat {
            return 20.0
        }
        
        static var topPaddingToItems: CGFloat {
            return 8.0
        }
        
        static var borderWidth: CGFloat {
            return 1.0
        }
        
        static var borderColor: UIColor {
            return UIColor.darkGray
        }
        
        static var horizontalEdgeInsetsValue: CGFloat {
            return 10.0
        }
        
        static var font: UIFont {
            return UIFont.systemFont(ofSize: textSize, weight: UIFont.Weight.semibold)
        }
        
        static var textSize: CGFloat {
            return 16.0
        }
        
        static var textColor: UIColor {
            return UIColor.white
        }
        
        static var cornerRadius: CGFloat {
            return 8.0
        }
        
        static var backgroundColor: UIColor {
            return UIColor.darkGray
        }
    }
    
    class BKDefaultButtonTheme: BKAlertButtonTheme {
        
        static var buttonHeight: CGFloat {
            return 40.0
        }
        
        static var topPaddingToTitle: CGFloat {
            return 20.0
        }
        
        static var topPaddingToItems: CGFloat {
            return 8.0
        }
        
        static var borderWidth: CGFloat {
            return 1.0
        }
        
        static var borderColor: UIColor {
            return UIColor.darkGray
        }
        
        static var horizontalEdgeInsetsValue: CGFloat {
            return 10.0
        }
        
        static var font: UIFont {
            return UIFont.systemFont(ofSize: textSize, weight: UIFont.Weight.regular)
        }
        
        static var textSize: CGFloat {
            return 16.0
        }
        
        static var textColor: UIColor {
            return UIColor.darkGray
        }
        
        static var cornerRadius: CGFloat {
            return 8.0
        }
        
        static var backgroundColor: UIColor {
            return UIColor.clear
        }
    }
    
    class BKDefaultWeakButtonTheme: BKAlertButtonTheme {
        
        static var buttonHeight: CGFloat {
            return 40.0
        }
        
        static var topPaddingToTitle: CGFloat {
            return 20.0
        }
        
        static var topPaddingToItems: CGFloat {
            return 8.0
        }
        
        static var borderWidth: CGFloat {
            return 0.0
        }
        
        static var borderColor: UIColor {
            return UIColor.clear
        }
        
        static var horizontalEdgeInsetsValue: CGFloat {
            return 10.0
        }
        
        static var font: UIFont {
            return UIFont.systemFont(ofSize: textSize, weight: UIFont.Weight.regular)
        }
        
        static var textSize: CGFloat {
            return 14.0
        }
        
        static var textColor: UIColor {
            return UIColor.lightGray
        }
        
        static var cornerRadius: CGFloat {
            return 0.0
        }
        
        static var backgroundColor: UIColor {
            return UIColor.clear
        }
    }
    
    class BKDefaultTitleTheme: BKAlertTextTheme {
        static var topPadding: CGFloat {
            return 20.0
        }
        
        static var font: UIFont {
            return UIFont.systemFont(ofSize: textSize, weight: UIFont.Weight.semibold)
        }
        
        static var textSize: CGFloat {
            return 24.0
        }
        
        static var textColor: UIColor {
            return UIColor.black
        }
        
        static var alignment: NSTextAlignment {
            return .center
        }
    }
    
    class BKDefaultSubtitleTheme: BKAlertTextTheme {
        static var topPadding: CGFloat {
            return 20.0
        }
        
        static var font: UIFont {
            return UIFont.systemFont(ofSize: textSize, weight: UIFont.Weight.regular)
        }
        
        static var textSize: CGFloat {
            return 18.0
        }
        
        static var textColor: UIColor {
            return UIColor.darkGray
        }
        
        static var alignment: NSTextAlignment {
            return .left
        }
        
        static var textVerticalPadding: CGFloat {
            return 14.0
        }
        
        static var topPaddingToTitle: CGFloat {
            return 0.0
        }
    }
    
    class BKDefaultTextTheme: BKAlertTextTheme {
        static var topPadding: CGFloat {
            return 20.0
        }
        
        static var font: UIFont {
            return UIFont.systemFont(ofSize: textSize, weight: UIFont.Weight.regular)
        }
        
        static var textSize: CGFloat {
            return 14.0
        }
        
        static var textColor: UIColor {
            return UIColor.lightGray
        }
        
        static var alignment: NSTextAlignment {
            return .left
        }
        
        static var textVerticalPadding: CGFloat {
            return 8.0
        }
    }
    
    static var strongButtonTheme: BKAlertButtonTheme.Type {
        return BKDefaultStrongButtonTheme.self
    }
    
    static var defaultButtonTheme: BKAlertButtonTheme.Type {
        return BKDefaultButtonTheme.self
    }
    
    static var weakButtonTheme: BKAlertButtonTheme.Type {
        return BKDefaultStrongButtonTheme.self
    }
    
    static var titleTheme: BKAlertTextTheme.Type {
        return BKDefaultTitleTheme.self
    }
    
    static var subtitleTheme: BKAlertTextTheme.Type {
        return BKDefaultSubtitleTheme.self
    }
    
    static var textTheme: BKAlertTextTheme.Type {
        return BKDefaultTextTheme.self
    }
    
    static func buttonThemeForStyle(_ style: BKAlertButtonStyle) -> BKAlertButtonTheme.Type {
        switch style {
        case .default:
            return defaultButtonTheme
        case .strong:
            return strongButtonTheme
        case .weak:
            return weakButtonTheme
        }
    }
    
    static func textThemeForStyle(_ style: BKAlertTextStyle) -> BKAlertTextTheme.Type {
        switch style {
        case .title:
            return titleTheme
        case .subtitle:
            return subtitleTheme
        case .text:
            return textTheme
        }
    }
    
    static var backgroundColor: UIColor {
        return UIColor.white
    }
    
    static var horizontalContainerPaddings: CGFloat {
        return 16.0
    }
    
    static var topContainerPadding: CGFloat {
        return 24.0
    }
    
    static var bottomContainerPadding: CGFloat {
        return 8.0
    }
    
    static var cornerRadius: CGFloat {
        return 10.0
    }
    
}

open class BKAlertDialog: UIView, CAAnimationDelegate {
    
    static var defaultFont:UIFont = UIFont.systemFont(ofSize: 14.0)
    
    private var created:Bool = false
    private var lastView:UIView?
    private var width:CGFloat = 300.0
    private var buttonSubview:UIView = UIView()
    private var orientation:ButtonOrientation?
    private var lastButton:UIButton?
    private var ow:UIWindow?
    private var contentView: UIView = UIView()
    
    private var cstBottomKeyboard: NSLayoutConstraint?
    
    private var theme: BKAlertDialogTheme.Type!
    
    var visible:Bool = false
    var style:UIStatusBarStyle = UIStatusBarStyle.lightContent
    open var viewController = UIViewController.init()
    
    func overlayWindow() ->UIWindow {
        if(self.ow == nil) {
            let _overlayWindow = UIWindow.init(frame: UIScreen.main.bounds)
            _overlayWindow.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
            _overlayWindow.backgroundColor = UIColor.clear
            _overlayWindow.windowLevel = UIWindow.Level.alert
            
            //self.viewController = UIViewController.init()
            //viewController.style = style
            _overlayWindow.rootViewController = viewController
            viewController.view.backgroundColor = UIColor.clear
            
            width = viewController.view.bounds.size.width - 80.0
            
            let fog = UIView()
            fog.translatesAutoresizingMaskIntoConstraints = false
            fog.backgroundColor = UIColor.init(white: 0.0, alpha: 0.5)
            viewController.view.addSubview(fog)
            viewController.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view":fog]))
            viewController.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view":fog]))
            
            let tap = UITapGestureRecognizer.init(target: self, action: #selector(closeDialog))
            fog.addGestureRecognizer(tap)
            
            self.ow = _overlayWindow;
        }
        
        return ow!;
        
    }
    
    public init(_ theme: BKAlertDialogTheme.Type? = nil) {
        super.init(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
        
        self.theme = theme ?? BKDefaultTheme.self
        
        self.isUserInteractionEnabled = true
        
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = 8.0
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.buttonSubview.isUserInteractionEnabled = true
        
        setupContent()
    }
    
    func setupContent() {
        
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(self.contentView)
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format: "H:|-0-[content(%f)]-0-|", width), options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["content": self.contentView]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[content]-0-|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["content": self.contentView]))
        
        self.addConstraint(NSLayoutConstraint(item: self.contentView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1.0, constant: 0.0))
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc func closeDialog() {
        self.endEditing(true)
        self.show(false)
    }
    
    func updateBackgroundColor(_ color:UIColor) -> BKAlertDialog {
        self.backgroundColor = color
        return self
    }
    
    open func addXib(_ view: UIView) -> BKAlertDialog {
        assert(self.lastView == nil, "Can't add a xib if other elements were already setup")
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear
        
        self.contentView.addSubview(view)
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[xib]-0-|", options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["xib": view]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[xib]-0-|", options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["xib": view]))
        
        self.contentView.layoutIfNeeded()
        
        lastView = view
        
        return self
    }
    
    open func addImageOutsideContainer(_ image: UIImage?) -> BKAlertDialog {
        if let image = image, lastView == nil {
            let imageView = UIImageView.init(image: image)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.layoutIfNeeded()
            self.contentView.addSubview(imageView)
            
            self.contentView.addConstraint(NSLayoutConstraint.init(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: self.contentView, attribute: .centerX, multiplier: 1.0, constant: 0.0))
            
            self.contentView.addConstraint(NSLayoutConstraint.init(item: imageView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1.0, constant: -imageView.bounds.size.height/2))
            
            lastView = imageView
        }
        
        self.setNeedsLayout()
        
        return self;
    }
    
    open func addImage(_ image:UIImage?, topPadding: CGFloat?) -> BKAlertDialog {
        
        if let image = image {
            let imageView = UIImageView.init(image: image)
            imageView.translatesAutoresizingMaskIntoConstraints = false;
            self.contentView.addSubview(imageView)
            
            self.contentView.addConstraint(NSLayoutConstraint(item:imageView, attribute: .centerX, relatedBy: .equal, toItem: self.contentView, attribute: .centerX, multiplier: 1.0, constant: 0))
            
            if(lastView != nil) {
                self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format:"V:[p]-%f-[view]", topPadding ?? 8.0), options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view": imageView, "p": lastView!]))
            } else {
                self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format:"V:|-%f-[view]", theme.topContainerPadding), options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view": imageView]))
            }
            
            lastView = imageView
        }
        
        self.setNeedsLayout()
        
        return self;
    }
    
    open func addText(_ title:String, style: BKAlertTextStyle) -> BKAlertDialog {
        
        let label = UILabel.init()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = title
        label.numberOfLines = 0
        
        let textTheme = theme.textThemeForStyle(style).self
        
        label.textAlignment = textTheme.alignment
        label.textColor = textTheme.textColor
        label.font = textTheme.font
        
        self.contentView.addSubview(label)
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format:"H:|-%f-[view]-%f-|", theme.horizontalContainerPaddings, theme.horizontalContainerPaddings), options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view": label]))
        
        if(lastView != nil) {
            self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format:"V:[p]-%f-[view]", textTheme.topPadding), options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view": label, "p": lastView!]))
        } else {
            self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format:"V:|-%f-[view]", textTheme.topPadding), options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view": label]))
        }
        lastView = label
        
        return self
    }
    
    open func setOrientation(_ orientation:ButtonOrientation) -> BKAlertDialog {
        assert(self.orientation == nil, "Can only set button orientation once")
        self.orientation = orientation
        return self
    }
    
    open func addButton(_ title:String, ico:UIImage? = nil, style: BKAlertButtonStyle, close: Bool = true, closure: ((_ v:UIView) -> (Void))?) -> BKAlertDialog {
        
        let buttonTheme = theme.buttonThemeForStyle(style)
        
        let button = AlertButton.init(buttonTheme, image: ico)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.clipsToBounds = true
        button.setTitle(title, for: .normal)
        button.setClosure(closure)
        button.setNeedsLayout()
        button.layoutIfNeeded()
        
        if close {
            button.addTarget(self, action: #selector(closeDialog), for: .touchUpInside)
        }
        
        self.orientation = self.orientation ?? ButtonOrientation.Horizontal
        
        if(buttonSubview.superview == nil) {
            buttonSubview.translatesAutoresizingMaskIntoConstraints = false
            self.contentView.addSubview(buttonSubview)
            
            self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format:"H:|-%f-[view]-%f-|", theme.horizontalContainerPaddings, theme.horizontalContainerPaddings), options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view": buttonSubview]))
            
            if(lastView != nil) {
                self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format: "V:[p]-%f-[view]", buttonTheme.topPaddingToTitle), options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view": buttonSubview, "p": lastView!]))
            } else {
                self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format: "V:|-%f-[view]", theme.topContainerPadding), options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view": buttonSubview]))
            }
        }
        
        buttonSubview.addSubview(button)
        
        if(self.orientation == ButtonOrientation.Horizontal) {
            
            buttonSubview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view(\(buttonTheme.buttonHeight))]-0-|", options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view": button]))
            
            if(lastButton != nil) {
                buttonSubview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[p]-10-[view]", options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view": button, "p": lastButton!]))
                buttonSubview.addConstraint(NSLayoutConstraint.init(item: button, attribute: .width, relatedBy: .equal, toItem: lastButton!, attribute: .width, multiplier: 1.0, constant: 0.0))
            } else {
                buttonSubview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]", options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view": button]))
            }
            
        } else {
            
            buttonSubview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[b]-0-|", options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["b":button]))
            
            if(lastButton != nil) {
                buttonSubview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[p]-\(buttonTheme.topPaddingToItems)-[view(\(buttonTheme.buttonHeight))]", options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view": button, "p": lastButton!]))
            } else {
                buttonSubview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view(\(buttonTheme.buttonHeight))]", options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view": button]))
            }
        }
        
        lastButton = button
        
        lastView = buttonSubview
        
        return self
    }
    
    override open var intrinsicContentSize:CGSize {
        get {
            if(self.subviews.count>0) {
                let v = self.subviews.last!
                let size = v.sizeThatFits(CGSize(width: width - theme.horizontalContainerPaddings*2, height: 10000))
                
                return CGSize(width:width, height:v.frame.origin.y + size.height + theme.horizontalContainerPaddings)
            }
            
            return CGSize(width:50, height:50)
        }
    }
    
    open func show(_ show:Bool) {
        
        if(!created) {
            
            for v in self.subviews {
                if let layouter = v as? DialogLayouter {
                    layouter.setWidth(width: width - theme.horizontalContainerPaddings*2)
                }
            }
            //add max width size
            self.addConstraint(NSLayoutConstraint(item: self, attribute: .width, relatedBy: .lessThanOrEqual, toItem: nil, attribute: .width, multiplier: 1.0, constant: width));
            
            //add constraint to button subview
            if(lastButton != nil) {
                if(orientation == ButtonOrientation.Horizontal) {
                    buttonSubview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[view]-0-|", options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view": lastButton!]))
                } else {
                    buttonSubview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view]-0-|", options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view": lastButton!]))
                }
            }
            
            self.setNeedsLayout()
            self.layoutIfNeeded()
            self.invalidateIntrinsicContentSize()
            
            self.created = true
            
        }
        
        if (self.visible == show && self.visible) {
            return;
        }
        
        self.visible = show;
        
        if (show) {
            
            self.layer.opacity = 0.0;
            
            _ = self.overlayWindow()
            
            if(self.superview == nil) {
                if lastView != nil {
                    self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format: "V:[p]-%f-|", theme.bottomContainerPadding), options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["p":lastView!]))
                }
                
                self.viewController.view.addSubview(self)
                self.viewController.view.addConstraint(NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: self.viewController.view, attribute: .centerX, multiplier: 1.0, constant: 0.0))
                
                let alignCenter = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: self.viewController.view, attribute: .centerY, multiplier: 1.0, constant: 0.0)
                alignCenter.priority = UILayoutPriority.init(500.0)
                
                self.viewController.view.addConstraint(alignCenter)
                
                cstBottomKeyboard = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: self.viewController.view, attribute: .bottom, multiplier: 1.0, constant: -10.0)
                
                self.viewController.view.addConstraint(cstBottomKeyboard!)
            }
            
            self.viewController.view.setNeedsLayout()
            self.viewController.view.layoutIfNeeded()
            self.viewController.view.invalidateIntrinsicContentSize()
            
            self.overlayWindow().makeKeyAndVisible()
            
            self._showAnimation()
        } else {
            
            self._hideAnimation()
        }
    }
    
    func deselectAll() {
        for v in self.subviews {
            if let option = v as? AlertOption {
                option.setSelected(false)
            }
        }
    }
    
    private func _showAnimation() {
        
        self.layer.removeAllAnimations()
        viewController.view.layer.removeAllAnimations()
        
        self.layer.opacity = 1.0
        let fade = CASpringAnimation.init(keyPath: "opacity")
        fade.fromValue = 0
        fade.toValue = 1
        fade.duration = 0.3
        
        let pop = CABasicAnimation.init(keyPath: "position.y");
        
        let center = UIScreen.main.bounds.size.height/2
        pop.fromValue = center
        pop.toValue = center
        pop.duration = 0.2
        
        
        let group = CAAnimationGroup.init();
        group.animations = [fade, pop];
        group.duration = 0.3
        
        self.layer.add(group, forKey: "pop")
        viewController.view.layer.add(fade, forKey: "fade")
    }
    
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if(flag) {
            _completionBlock()
        }
    }
    
    private var _isHiding = false
    private func _hideAnimation() {
        
        if(!_isHiding) {
            _isHiding = true
            self.layer.removeAllAnimations()
            viewController.view.layer.removeAllAnimations()
            
            self.layer.opacity = 0.0
            
            let fade = CABasicAnimation.init(keyPath: "opacity")
            
            fade.fromValue = 1
            fade.toValue = 0
            fade.duration = 0.35;
            
            
            let pop = CABasicAnimation.init(keyPath: "position.y");
            let center = UIScreen.main.bounds.size.height/2
            pop.fromValue = center
            pop.toValue = center
            pop.duration = 0.3
            
            let group = CAAnimationGroup.init();
            group.animations = [fade, pop];
            group.duration = 0.3
            
            group.delegate = self;
            
            self.layer.add(group, forKey: "pop")
            viewController.view.layer.add(fade, forKey: "fade")
        }
    }
    
    private func _completionBlock() {
        //viewController.view.hidden = true;
        //self.layer.removeAllAnimations()
        
        _isHiding = false
        self.removeFromSuperview()
        ow?.isHidden = true
        self.ow = nil
        
        self._activateAppWindow()
    }
    
    private func _activateAppWindow() {
        
        for (_,window) in UIApplication.shared.windows.enumerated().reversed() {
            if(window.windowLevel == UIWindow.Level.normal) {
                window.makeKey()
                return;
            }
        }
    }
    
    // MARK: - Keyboard
    
    open func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    open func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillChangeFrame(notification: Notification) {
        if let cst = cstBottomKeyboard,
            let dict = notification.userInfo,
            let frame = dict[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect,
            let duration = dict[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval
        {
            
            var offset: CGFloat = 0
            if frame.height == 0 {
                offset = 0
            } else {
                if let window = UIApplication.shared.keyWindow {
                    if #available(iOS 11.0, *) {
                        offset = window.safeAreaInsets.bottom
                    }
                }
            }
            
            UIView.animate(withDuration: duration, animations: {
                cst.constant = -(frame.size.height - offset)
                self.viewController.view.layoutIfNeeded()
            }, completion: nil)
            
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        if let cst = cstBottomKeyboard,
            let dict = notification.userInfo,
            let duration = dict[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval
        {
            UIView.animate(withDuration: duration, animations: {
                cst.constant = -10
                self.viewController.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    deinit {
        unregisterKeyboardNotifications()
    }
    
}

open class AlertOption:UIView,DialogLayouter {
    
    private var leftIconInset:CGFloat = 20.0
    private let rightIconInset: CGFloat = 16.0
    private let rightTextInset: CGFloat = 20.0
    private var image:UIImage?
    private var selectedImage:UIImage?
    private var _isSelected:Bool = false
    private var width:CGFloat = 300
    private var font: UIFont?
    private var color: UIColor?
    private var imgSize: CGFloat = 20.0
    
    private lazy var title:UILabel = {
        let _textLabel = UILabel()
        _textLabel.font = (self.font != nil) ? self.font : UIFont.systemFont(ofSize: 14.0)
        _textLabel.textColor = self.color != nil ? self.color : UIColor.black
        _textLabel.backgroundColor = UIColor.clear
        _textLabel.textAlignment = .left
        _textLabel.lineBreakMode = .byWordWrapping
        _textLabel.numberOfLines = 0
        
        _textLabel.translatesAutoresizingMaskIntoConstraints = false
        
        
        return _textLabel
    }()
    
    private lazy var imageView:UIImageView = {
        let _imageView = UIImageView.init()
        _imageView.contentMode = .center
        _imageView.translatesAutoresizingMaskIntoConstraints = false
        _imageView.setContentHuggingPriority(UILayoutPriority.init(251.0), for: .horizontal)
        _imageView.setContentCompressionResistancePriority(UILayoutPriority.init(751), for: .horizontal)
        return _imageView
    }()
    
    internal var c:((AlertOption) -> (Void))?
    
    init(font: UIFont?, color: UIColor?, leftInset: CGFloat?, imgSize: CGFloat?) {
        super.init(frame: CGRect.zero)
        
        self.font = font
        self.color = color
        
        if leftInset != nil {
            self.leftIconInset = leftInset!
        }
        
        if imgSize != nil {
            self.imgSize = imgSize!
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(title)
        self.imageView.contentMode = .scaleAspectFit
        self.addSubview(imageView)
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format:"H:|-%f-[image(\(self.imgSize))]-%f-[title]-%f-|", leftIconInset, rightIconInset, rightTextInset), options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["image":imageView, "title":title]))
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[imgView(\(self.imgSize))]-0-|", options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["imgView": imageView]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-2-[title]-(>=0)-|", options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["title":title]))
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(click))
        self.addGestureRecognizer(tap)
        self.isUserInteractionEnabled = true
    }
    
    func setTitle(_ title:String) {
        self.title.text = title
    }
    
    func setImage(_ image:UIImage) {
        self.image = image
        self.imageView.image = image
    }
    
    func setSelectedImage(_ image:UIImage) {
        self.selectedImage = image
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setClosure(_ closure:((_ v:AlertOption) -> (Void))? ) {
        self.c = closure
    }
    
    @objc func click() {
        if(c != nil) {
            c!(self)
        }
    }
    
    func setSelected(_ selected:Bool) {
        self._isSelected = selected
        if(_isSelected) {
            self.imageView.image = selectedImage
        } else {
            self.imageView.image = image
        }
    }
    
    func isSelected() -> Bool {
        return _isSelected
    }
    
    
    override open var intrinsicContentSize:CGSize {
        //let imageSize = imageView.intrinsicContentSize()
        //let labelSize = title.intrinsicContentSize()
        //return CGSizeMake(imageSize.width + labelSize.width + textInset, max(imageSize.height, labelSize.height))
        get {
            return sizeThatFits(CGSize.init(width: width, height: 1))
        }
    }
    
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        let imageSize = imageView.intrinsicContentSize
        
        let labelSize = title.sizeThatFits(CGSize.init(width: size.width - imageSize.width - rightTextInset, height: 10000))
        
        return CGSize.init(width: size.width, height: max(imageSize.height, labelSize.height))
    }
    
    func setWidth(width: CGFloat) {
        self.width = width
        self.invalidateIntrinsicContentSize()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

class AlertButton:BKRoundButton {
    
    private let imageSpacing: CGFloat = 10
    private var c:((UIButton) -> (Void))?
    
    init() {
        super.init(frame: CGRect.zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.titleLabel?.font = UIFont.systemFont(ofSize: 14.0, weight: UIFont.Weight.semibold)
        self.titleLabel?.numberOfLines = 2
        self.titleLabel?.textAlignment = .center
        self.borderColor = UIColor.black
        self.borderWidth = 1.0
        self.cornerRadius = -1.0
        self.backgroundColor = UIColor.white
        self.setTitleColor(UIColor.black, for: .normal)
        
        addTarget(self, action: #selector(click), for: .touchUpInside)
    }
    
    init(_ theme: BKAlertButtonTheme.Type, image: UIImage?) {
        super.init(frame: CGRect.zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.titleLabel?.font = theme.font
        self.titleLabel?.numberOfLines = 2
        self.titleLabel?.textAlignment = .center
        self.borderColor = theme.borderColor
        self.borderWidth = theme.borderWidth
        self.cornerRadius = theme.cornerRadius
        self.backgroundColor = theme.backgroundColor
        self.setTitleColor(theme.textColor, for: .normal)
        
        if let img = image {
            self.setImage(img, for: .normal)
            self.contentEdgeInsets = UIEdgeInsets.init(top: 10, left: 10 + imageSpacing, bottom: 10, right: 10 + imageSpacing)
        } else {
            self.contentEdgeInsets = UIEdgeInsets.init(top: 10, left: theme.horizontalEdgeInsetsValue, bottom: 10, right: theme.horizontalEdgeInsetsValue)
        }
        
        addTarget(self, action: #selector(click), for: .touchUpInside)
    }
    
    
    func setClosure(_ closure:((_ v:UIButton) -> (Void))? ) {
        self.c = closure
    }
    
    @objc func click() {
        if(c != nil) {
            c!(self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
        let rect = super.titleRect(forContentRect: contentRect)
        let imageRect = super.imageRect(forContentRect: contentRect)
        
        //text start where image started
        if(imageRect.size.width == 0) {
            return rect
        } else {
            return CGRect.init(x: imageRect.minX-imageSpacing/2, y:rect.minY, width:rect.width + imageSpacing*2, height: rect.height)
        }
    }
    
    override func imageRect(forContentRect contentRect: CGRect) -> CGRect {
        let rect = super.titleRect(forContentRect: contentRect)
        let imageRect = super.imageRect(forContentRect: contentRect)
        
        //image starts after content now
        
        return CGRect.init(x: rect.maxX - imageRect.width + imageSpacing/2, y: imageRect.minY, width: imageRect.width, height: imageRect.height)
    }
}
