//
//  BKRoundView.swift
//  InterfaceKit
//
//  Created by Rogerio Shimizu on 04/01/18.
//  Copyright © 2018 Topsports Ventures S.A. All rights reserved.
//

import UIKit

@IBDesignable
open class BKRoundView: UIView {

    @IBInspectable open var cornerRadius:CGFloat = 0 {
        didSet {
            redrawCorners()
        }
    }
    @IBInspectable open var borderColor:UIColor? {
        didSet {
            redrawCorners()
        }
    }
    @IBInspectable open var borderWidth:CGFloat = 0 {
        didSet {
            redrawCorners()
        }
    }
    @IBInspectable open var cornerTL:Bool = true {
        didSet {
            redrawCorners()
        }
    }
    @IBInspectable open var cornerTR:Bool = true {
        didSet {
            redrawCorners()
        }
    }
    @IBInspectable open var cornerBL:Bool = true {
        didSet {
            redrawCorners()
        }
    }
    @IBInspectable open var cornerBR:Bool = true {
        didSet {
            redrawCorners()
        }
    }
    
    private var borderLayer:CAShapeLayer?
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        self.clipsToBounds = true
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        redrawCorners()
    }
    
    func redrawCorners() {
        var corners:UIRectCorner = []
    
        if cornerTL {
            corners.insert(.topLeft)
        }
        if cornerTR {
            corners.insert(.topRight)
        }
        if cornerBL {
            corners.insert(.bottomLeft)
        }
        if cornerBR {
            corners.insert(.bottomRight)
        }
        
        let mask = CAShapeLayer()
        let shadowPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize.init(width: cornerRadius, height: cornerRadius))
        mask.path = shadowPath.cgPath
        
        self.layer.mask = mask
        
        if borderLayer != nil {
            borderLayer?.removeFromSuperlayer()
        }
        
        self.borderLayer = CAShapeLayer()
        borderLayer?.path = shadowPath.cgPath
        borderLayer?.lineWidth = borderWidth
        borderLayer?.strokeColor = borderColor?.cgColor
        borderLayer?.fillColor = UIColor.clear.cgColor
        
        borderLayer?.frame = self.bounds
        self.layer.addSublayer(borderLayer!)
    }
}
