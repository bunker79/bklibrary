//
//  BKSwitch.swift
//  InterfaceKit
//
//  Created by Rogerio Shimizu on 05/10/18.
//  Copyright © 2018 Bunker79. All rights reserved.
//

import UIKit

@IBDesignable
open class BKSwitch: BKTextSwitch {
    
    @IBInspectable
    open var labelOn: String = " " {
        didSet {
            titleCollection = "\(labelOn)|\(labelOff)"
        }
    }
    
    @IBInspectable
    open var labelOff: String = " " {
        didSet {
            titleCollection = "\(labelOn)|\(labelOff)"
        }
    }
    
    @IBInspectable
    open var offColor: UIColor = UIColor.white {
        didSet {
            if !isOn {
                backgroundColor = offColor
            }
        }
    }
    
    @IBInspectable
    open var onColor: UIColor = UIColor.white {
        didSet {
            if isOn {
                backgroundColor = onColor
            }
        }
    }
    
    @IBInspectable
    open var isOn: Bool = true {
        didSet {
            setSelectedIndex(isOn ? 1 : 0, animated: false)
        }
    }
    
    open override func setSelectedIndex(_ selectedIndex: Int, animated: Bool) {
        switch selectedIndex {
        case 0:
            targetColor = offColor
        default:
            targetColor = onColor
        }
        
        super.setSelectedIndex(selectedIndex, animated: animated)
    }
    
    
    
}
