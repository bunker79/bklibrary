//
//  BKGradientPointView.swift
//  BKLibrary
//
//  Created by André Assis on 01/02/18.
//  Copyright © 2018 BUNKER79. All rights reserved.
//

import UIKit

@IBDesignable
class BKGradientPointView: UIView {
    
    var gradient:CAGradientLayer = CAGradientLayer()
    
    @IBInspectable open var color1:UIColor? {
        didSet {
            updateGradient()
        }
    }
    @IBInspectable open var color2:UIColor? {
        didSet {
            updateGradient()
        }
    }
    
    @IBInspectable open var location1: CGFloat = 0.0 {
        didSet {
            updateGradient()
        }
    }
    
    @IBInspectable open var location2: CGFloat = 0.0 {
        didSet {
            updateGradient()
        }
    }
    
    @IBInspectable open var startPointX: CGFloat = 0.0 {
        didSet {
            updateGradient()
        }
    }
    
    @IBInspectable open var startPointY: CGFloat = 0.0 {
        didSet {
            updateGradient()
        }
    }
    
    @IBInspectable open var endPointX: CGFloat = 0.0 {
        didSet {
            updateGradient()
        }
    }
    
    @IBInspectable open var endPointY: CGFloat = 0.0 {
        didSet {
            updateGradient()
        }
    }

    func updateGradient() {
        gradient.frame = self.bounds
        if let color1 = color1, let color2 = color2 {
            gradient.colors = [
                color1.cgColor,
                color2.cgColor
            ]
            gradient.locations = [NSNumber.init(value: Float(location1)), NSNumber(value: Float(location2))]
            gradient.startPoint = CGPoint(x: startPointX, y: startPointY)
            gradient.endPoint = CGPoint(x: endPointX, y: endPointY)
            if gradient.superlayer == nil {
                layer.insertSublayer(gradient, at: 0)
            }
        } else {
            gradient.removeFromSuperlayer()
        }
    }

}
