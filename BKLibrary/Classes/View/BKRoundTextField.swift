//
//  BKRoundTextField.swift
//  BKLibrary
//
//  Created by André Assis on 15/03/19.
//

import UIKit

@IBDesignable
open class BKRoundTextField: UITextField {
    
    @IBInspectable open var placeholderColor: UIColor? {
        didSet {
            if let color = placeholderColor {
                self.attributedPlaceholder = NSAttributedString.init(string: self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : color, NSAttributedString.Key.font : self.font ?? UIFont.systemFont(ofSize: 16.0)])
            }
        }
    }
    
    @IBInspectable open var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable open var borderWidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable open var borderColor: UIColor? {
        didSet {
            self.layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable open var topEdgeInset: CGFloat = 0.0
    @IBInspectable open var bottomEdgeInset: CGFloat = 0.0
    @IBInspectable open var leftEdgeInset: CGFloat = 0.0
    @IBInspectable open var rightEdgeInset: CGFloat = 0.0
    
    override open func awakeFromNib() {
        super.awakeFromNib()
    }
    
    open override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: topEdgeInset, left: leftEdgeInset, bottom: bottomEdgeInset, right: rightEdgeInset))
    }
    
    open override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: topEdgeInset, left: leftEdgeInset, bottom: bottomEdgeInset, right: rightEdgeInset))
    }
    
    open override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets(top: topEdgeInset, left: leftEdgeInset, bottom: bottomEdgeInset, right: rightEdgeInset))
    }
    
}
