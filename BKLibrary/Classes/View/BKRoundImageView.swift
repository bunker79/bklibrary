//
//  BKRoundImageView.swift
//  BKLibrary
//
//  Created by Rogerio Shimizu on 09/01/18.
//  Copyright © 2018 BUNKER79. All rights reserved.
//

import UIKit

@IBDesignable
open class BKRoundImageView: UIImageView {
    
    @IBInspectable open var cornerRadius:CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable open var borderWidth:CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    @IBInspectable open var borderColor:UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
}
