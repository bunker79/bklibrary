//
//  BKActivationButton.swift
//  BKLibrary
//
//  Created by André Assis on 24/01/18.
//  Copyright © 2018 BUNKER79. All rights reserved.
//

import UIKit

@IBDesignable
public class BKActivationButton: BKRoundButton {

    @IBInspectable public var disabledBackgroundColor: UIColor?
    
    private var originalBackgroundColor: UIColor?
    
    public override var isEnabled: Bool {
        didSet {
            UIView.transition(with: self, duration: 0.2, options: .transitionCrossDissolve, animations: { [weak self] in
                if let s = self {
                    s.backgroundColor = s.isEnabled ? s.originalBackgroundColor : s.disabledBackgroundColor
                }
            }, completion: nil)
        }
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.originalBackgroundColor = self.backgroundColor
    }
    
    deinit {
        
    }

}
