//
//  FakeCellWithSeparatorView.swift
//
//  Created by Rogerio Shimizu on 20/09/17.
//  Copyright © 2017 BUNKER79. All rights reserved.
//

import UIKit

@IBDesignable
class BKFakeCellWithSeparatorView: UIView {
    
    @IBInspectable var color:UIColor? {
        didSet {
            line?.backgroundColor = color
        }
    }
    @IBInspectable var offset:CGFloat = 16 {
        didSet {
            addLine()
        }
    }
    
    var line:UIView?
    
    
    func addLine() {
        if line != nil {
            line?.removeFromSuperview()
        }
        
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = color
        self.addSubview(view)
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format:"H:|-%f-[view]-0-|", offset), options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["view": view]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view(1)]-0-|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: ["view": view]))
        
        line = view
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if line == nil {
            addLine()
        }
    }

}
