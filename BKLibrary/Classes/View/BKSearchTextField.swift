//
//  BKTextField.swift
//  BKLibrary
//
//  Created by André Assis on 23/01/18.
//  Copyright © 2018 BUNKER79. All rights reserved.
//

import UIKit

public protocol BKSearchTextFieldDelegate {
    func accessoryAction(_ textField: BKSearchTextField)
}

@IBDesignable
public class BKSearchTextField: UITextField {
    
    @IBInspectable var cancelImage: UIImage?
    @IBInspectable var actionEnabled: Bool = true
    
    public override var text: String? {
        didSet {
            if let img = cancelImage {
                if let t = text, t.count > 0 {
                    imageView.image = img
                } else {
                    imageView.image = image
                }
            }
        }
    }
    
    @IBInspectable var image: UIImage? {
        didSet {
            imageView.image = image
        }
    }
    
    @IBInspectable var underlineColor: UIColor? {
        didSet {
            underlineView.backgroundColor = underlineColor
        }
    }
    
    public var bkFieldDelegate: BKSearchTextFieldDelegate?
    
    private let defaultImageSize: CGFloat = 44.0
    private let imageView = UIImageView()
    private let underlineView = UIView()
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        underlineView.translatesAutoresizingMaskIntoConstraints = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(textDidChange), name: UITextField.textDidChangeNotification, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(begunEditing), name: NSNotification.Name.UITextFieldTextDidBeginEditing, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(endedEditing), name: NSNotification.Name.UITextFieldTextDidEndEditing, object: nil)
        
        self.addSubview(underlineView)
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view": underlineView]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view(1)]-0-|", options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["view": underlineView]))
        
        self.enablesReturnKeyAutomatically = true
        
        if image != nil || cancelImage != nil {
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.isUserInteractionEnabled = true
            imageView.contentMode = .center
            
            self.addSubview(imageView)
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[imageView(\(defaultImageSize))]-(-14)-|", options: NSLayoutConstraint.FormatOptions.init(rawValue: 0), metrics: nil, views: ["imageView": imageView]))
            self.addConstraint(NSLayoutConstraint.init(item: imageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: defaultImageSize))
            self.addConstraint(NSLayoutConstraint.init(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0.0))
            
            let tap = UITapGestureRecognizer.init(target: self, action: #selector(fieldAccessoryAction))
            imageView.addGestureRecognizer(tap)
        }
    }
    
    public func setRegularStateImage() {
        imageView.image = image
    }
    
    /*@objc private func begunEditing() {
        if let img = cancelImage {
            if let t = text, t.count > 0 {
                imageView.image = img
            }
        }
    }
    
    @objc private func endedEditing() {
        if cancelImage != nil && image != nil {
            imageView.image = image
        }
    }*/
    
    @objc private func textDidChange() {
        if let img = cancelImage {
            if let t = text, t.count > 0 {
                imageView.image = img
            } else {
                imageView.image = image
            }
        }
    }
    
    @objc private func fieldAccessoryAction() {
        if actionEnabled {
            bkFieldDelegate?.accessoryAction(self)
            
            if cancelImage != nil {
                imageView.image = image
            }
        }
    }
    
    public override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x,
                      y: bounds.origin.y,
                      width: image != nil ? bounds.size.width - defaultImageSize : bounds.size.width,
                      height: bounds.size.height)
    }
    
    public override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x,
                      y: bounds.origin.y,
                      width: image != nil ? bounds.size.width - defaultImageSize : bounds.size.width,
                      height: bounds.size.height)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UITextField.textDidChangeNotification, object: nil)
        //NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UITextFieldTextDidBeginEditing, object: nil)
        //NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UITextFieldTextDidEndEditing, object: nil)
    }
    
}
