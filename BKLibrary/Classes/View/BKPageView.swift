//
//  BKPageView.swift
//  BKLibrary
//
//  Created by Rogerio Shimizu on 10/01/18.
//  Copyright © 2018 BUNKER79. All rights reserved.
//

import UIKit

@IBDesignable
public class BKPageView: UIView {

    @IBInspectable var spacing:Int = 0 {
        didSet {
            rebuildInterface()
        }
    }
    @IBInspectable var defaultImage:UIImage! {
        didSet {
            rebuildInterface()
        }
    }
    @IBInspectable var selectedImage:UIImage! {
        didSet {
            rebuildInterface()
        }
    }
    @IBInspectable public var numberOfItems:Int = 2 {
        didSet {
            rebuildInterface()
        }
    }
    
    @IBInspectable public var selectedIndex:Int = 0 {
        didSet {
            rebuildInterface()
        }
    }
    
    var selectedItem:Int = 0
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    private func rebuildInterface() {
        _ = subviews.map { (v) -> Void in
            v.removeFromSuperview()
        }
        
        var lastItem:UIView? = nil
        
        for i in 0..<numberOfItems {
            let button = UIButton.init(frame: .zero)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.setImage(defaultImage, for: .normal)
            button.setImage(selectedImage, for: .selected)
            button.setTitle("", for: .normal)
            self.addSubview(button)
            
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options: NSLayoutConstraint.FormatOptions(rawValue:0), metrics: nil, views: ["view": button]))
            
            if let item = lastItem {
                self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: String(format:"H:[item]-%ld-[view]", spacing), options: NSLayoutConstraint.FormatOptions(rawValue:0), metrics: nil, views: ["view": button, "item": item]))
                
            } else {
                self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]", options: NSLayoutConstraint.FormatOptions(rawValue:0), metrics: nil, views: ["view": button]))
            }
            
            lastItem = button
            
            if i == selectedIndex {
                button.isSelected = true
            }
            
        }
        
        if let item = lastItem {
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[view]-0-|", options: NSLayoutConstraint.FormatOptions(rawValue:0), metrics: nil, views: ["view": item]))
        }
        
        invalidateIntrinsicContentSize()
    }
    
    
    public override var intrinsicContentSize: CGSize {
        let h = defaultImage.size.height
        let w = defaultImage.size.width
        let n = CGFloat(numberOfItems)
        let s = CGFloat(spacing)
        return CGSize.init(width: w*n + (n-1)*s, height: h)
    }
    
    
    
    

}
