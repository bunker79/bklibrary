//
//  BKGradientView.swift
//  BKLibrary
//
//  Created by Rogerio Shimizu on 26/01/18.
//  Copyright © 2018 BUNKER79. All rights reserved.
//

import UIKit

@IBDesignable
open class BKGradientView: BKRoundView {

    var gradient:CAGradientLayer = CAGradientLayer()
    
    @IBInspectable open var color1:UIColor? {
        didSet {
            updateGradient()
        }
    }
    @IBInspectable open var color2:UIColor? {
        didSet {
            updateGradient()
        }
    }
    
    @IBInspectable open var horizontal:Bool = true {
        didSet {
            updateGradient()
        }
    }
    
    @IBInspectable open var vertical:Bool = false {
        didSet {
            updateGradient()
        }
    }
    
    func updateGradient() {
        gradient.frame = self.bounds
        if let color1 = color1, let color2 = color2 {
            gradient.colors = [
                color1.cgColor,
                color2.cgColor
            ]
            /*gradient.locations = [0, 1]
            gradient.startPoint = CGPoint(x: 0.5, y: 0)
            gradient.endPoint = CGPoint(x: 0.5, y: 1)*/
            if gradient.superlayer == nil {
                layer.insertSublayer(gradient, at: 0)
            }
            if let mask = self.layer.mask {
                gradient.mask = mask
            }
        } else {
            gradient.removeFromSuperlayer()
        }
    }

    open override func layoutSubviews() {
        super.layoutSubviews()
        updateGradient()
    }
}

