//
//  BKShadowView.swift
//  BKLibrary
//
//  Created by André Assis on 24/01/18.
//  Copyright © 2018 BUNKER79. All rights reserved.
//

import UIKit

@IBDesignable
public class BKShadowView: BKRoundView {
    
    @IBInspectable var offsetHeight: CGFloat = 0.0
    @IBInspectable var offsetWidth: CGFloat = 0.0
    
    @IBInspectable var color: UIColor? {
        didSet {
            self.layer.shadowColor = color?.cgColor
        }
    }
    
    @IBInspectable var opacity: CGFloat = 0.0 {
        didSet {
            self.layer.shadowOpacity = Float(opacity)
        }
    }
    
    @IBInspectable var radius: CGFloat = 0.0 {
        didSet {
            self.layer.shadowRadius = radius
        }
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        self.layer.shadowOffset = CGSize(width: offsetWidth, height: offsetHeight)
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
