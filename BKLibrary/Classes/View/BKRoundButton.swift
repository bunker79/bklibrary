//
//  BKRoundButton.swift
//  BKLibrary
//
//  Created by Rogerio Shimizu on 12/01/18.
//  Copyright © 2018 BUNKER79. All rights reserved.
//

import UIKit

@IBDesignable
public class BKRoundButton: UIButton {

    @IBInspectable open var cornerRadius:CGFloat = 0 {
        didSet {
            redrawCorners()
        }
    }
    @IBInspectable open var borderWidth:CGFloat = 0 {
        didSet {
            redrawCorners()
        }
    }
    @IBInspectable open var borderColor:UIColor? {
        didSet {
            redrawCorners()
        }
    }
    @IBInspectable open var cornerTL:Bool = true {
        didSet {
            redrawCorners()
        }
    }
    @IBInspectable open var cornerTR:Bool = true {
        didSet {
            redrawCorners()
        }
    }
    @IBInspectable open var cornerBL:Bool = true {
        didSet {
            redrawCorners()
        }
    }
    @IBInspectable open var cornerBR:Bool = true {
        didSet {
            redrawCorners()
        }
    }
    
    private var borderLayer:CAShapeLayer?
    
    @IBInspectable open var selectedBgColor: UIColor? = nil {
        didSet {
            updateSelectedColors()
        }
    }
    
    @IBInspectable open var selectedBorderColor: UIColor? = nil {
        didSet {
            updateSelectedColors()
        }
    }
    
    @IBInspectable open var disabledBgColor: UIColor? = nil {
        didSet {
            updateDisabledColors()
        }
    }
    
    @IBInspectable open var disabledBorderColor: UIColor? = nil {
        didSet {
            updateDisabledColors()
        }
    }
    
    @IBInspectable open var originalBgColor: UIColor?
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        self.clipsToBounds = true
        
        if isSelected {
            updateSelectedColors()
        }
        
        if !isEnabled {
            updateDisabledColors()
        }
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        redrawCorners()
    }
    
    func redrawCorners() {
        var corners:UIRectCorner = []
        
        if cornerTL {
            corners.insert(.topLeft)
        }
        if cornerTR {
            corners.insert(.topRight)
        }
        if cornerBL {
            corners.insert(.bottomLeft)
        }
        if cornerBR {
            corners.insert(.bottomRight)
        }
        
        if corners == [] {
            corners = [.allCorners]
        }
        
        let mask = CAShapeLayer()
        let shadowPath = UIBezierPath.init(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize.init(width: cornerRadius, height: cornerRadius))
        mask.path = shadowPath.cgPath
        
        self.layer.mask = mask
        
        if borderLayer != nil {
            borderLayer?.removeFromSuperlayer()
        }
        
        self.borderLayer = CAShapeLayer()
        borderLayer?.path = shadowPath.cgPath
        borderLayer?.lineWidth = borderWidth
        borderLayer?.strokeColor = borderColor?.cgColor
        borderLayer?.fillColor = UIColor.clear.cgColor
        
        borderLayer?.frame = self.bounds
        self.layer.addSublayer(borderLayer!)
    }
    
    public override var isSelected: Bool {
        didSet {
            updateSelectedColors()
        }
    }
    
    private func updateSelectedColors() {
        UIView.animate(withDuration: 0.2) {
            self.backgroundColor = self.isSelected ? (self.selectedBgColor != nil ? self.selectedBgColor : self.originalBgColor) : self.originalBgColor
            self.layer.borderColor = self.isSelected ? (self.selectedBorderColor != nil ? self.selectedBorderColor?.cgColor : self.borderColor?.cgColor) : self.borderColor?.cgColor
        }
    }
    
    public override var isEnabled: Bool {
        didSet {
            updateDisabledColors()
        }
    }
    
    private func updateDisabledColors() {
        UIView.animate(withDuration: 0.2) {
            self.backgroundColor = !self.isEnabled ? (self.disabledBgColor != nil ? self.disabledBgColor : self.originalBgColor) : self.originalBgColor
            self.layer.borderColor = !self.isEnabled ? (self.disabledBorderColor != nil ? self.disabledBorderColor?.cgColor : self.borderColor?.cgColor) : self.borderColor?.cgColor
        }
    }

}
