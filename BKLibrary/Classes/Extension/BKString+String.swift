//
//  StringUtils.swift
//  BKLibrary
//
//  Created by Rogerio Shimizu on 19/01/18.
//  Copyright © 2018 BUNKER79. All rights reserved.
//

import Foundation


public extension String {
    
    func decimalValue() -> NSDecimalNumber {
        let result = String(self.filter { "01234567890".contains($0) })
        //result is number only
        if result.count > 0 {
            let n = Double(result)! / 100
            return NSDecimalNumber.init(value: n)
        } else {
            return 0
        }
    }
    
    func reformatAsCurrency() -> String {
        //remove all bogus characters
        return self.decimalValue().asCurrency
    }
    
    func validateEmail() -> Bool {
        let checkString = self as NSString
        
        let laxString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailRegex = laxString
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        
        return emailTest.evaluate(with: checkString)
    }
    
    func validatePhoneNumber() -> Bool {
        let checkString = self as NSString
        
        let regex = "^\\(([1-9]{2})\\) (([2-5]{1}|[9]{1}[1-9]{1})[0-9]{3}|(70|77|78|79)[0-9]{2})\\-[0-9]{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", regex)
        
        return phoneTest.evaluate(with: checkString)
    }
    
    func removeNotDigitCharacters() -> String {
        let doNotWant = CharacterSet.decimalDigits.inverted
        let formattedString = self.components(separatedBy: doNotWant).joined()
        
        return formattedString
    }
    
    func formattedPhoneNumberString() -> String {
        var sourceString = self.removeNotDigitCharacters()
        
        if sourceString.count == 12 || sourceString.count == 13 {
            sourceString = self.replacingOccurrences(of: "55", with: "")
        }
        
        var preffixLength: Int?
        
        if sourceString.count == 10 {
            preffixLength = 4
        } else if sourceString.count == 11 {
            preffixLength = 5
        }
        
        if let length = preffixLength {
            let areaCodeString = sourceString.newSubstring(0, length: 2)
            let preffixString = sourceString.newSubstring(areaCodeString.count, length: length)
            let suffixString = sourceString.newSubstring(areaCodeString.count + preffixString.count, length: 4)
            
            let formattedPhoneString = "(\(areaCodeString)) \(preffixString)-\(suffixString)"
            
            if formattedPhoneString.validatePhoneNumber() {
                return formattedPhoneString
            }
        }
        
        return self
    }
    
    func newSubstring(_ start:Int, length: Int) -> String {
        let startIndex = self.index(self.startIndex, offsetBy: start)
        let endIndex = self.index(startIndex, offsetBy: length-1)
        
        return String.init(self[startIndex...endIndex])
    }
    
    func diacriticString() -> String {
        return self.folding(options: .diacriticInsensitive, locale: .current)
    }
    
    
}
