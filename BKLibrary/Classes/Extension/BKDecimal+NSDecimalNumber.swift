//
//  StringUtils.swift
//  BKLibrary
//
//  Created by Rogerio Shimizu on 19/01/18.
//  Copyright © 2018 BUNKER79. All rights reserved.
//

import Foundation

public extension NSDecimalNumber {
    
    var asCurrency: String {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        
        return formatter.string(from: self)!
    }
       
}
