
//
//  BKVersionHelper.swift
//  BKLibrary
//
//  Created by Rogerio Shimizu on 09/01/18.
//  Copyright © 2018 BUNKER79. All rights reserved.
//

import Foundation

public class BKVersionHelper {
    
    /**
    *
    * Return iOS version as String
    **/
    public class func version() -> String {
        
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        
    }
    
}
