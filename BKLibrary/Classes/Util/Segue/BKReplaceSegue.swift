//
//  BKReplaceSegue.swift
//  BKLibrary
//
//  Created by Rogerio Shimizu on 11/01/18.
//  Copyright © 2018 BUNKER79. All rights reserved.
//

import UIKit

class BKReplaceSegue: UIStoryboardSegue {

    override func perform() {
        self.source.navigationController?.setViewControllers([destination], animated: true)
    }
    
}
