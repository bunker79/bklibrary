//
//  BKArraySequenceUtils.swift
//  BKLibrary
//
//  Created by André Assis on 23/01/18.
//  Copyright © 2018 BUNKER79. All rights reserved.
//

import Foundation

extension Array {
    
    public func itemFor(index: IndexPath) -> Element? {
        if index.row < self.count && index.row >= 0 {
            return self[index.row]
        }
        return nil
    }
    
    public func itemFor(index: Int) -> Element? {
        if index < self.count && index >= 0 {
            return self[index]
        }
        return nil
    }
    
}
