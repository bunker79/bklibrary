#
# Be sure to run `pod lib lint BKLibrary.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BKLibrary'
  s.version          = '0.6.6'
  s.summary          = 'A collection of useful classes (mostly UI) to help building interfaces.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
We use this Pod to keep useful classes that are used in almost every project we do currently. 
The core contains most UI classes. You can opt in other stuff, like extensions or video players
                       DESC

  s.homepage         = 'https://github.com/rojas/BKLibrary'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Rogerio Shimizu' => 'roja@bunker79.com' }
  s.source           = { :git => 'https://github.com/rojas/BKLibrary.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'
  s.swift_version = '5.0'

  s.source_files = 'BKLibrary/Classes/**/*'
  
  s.resources = 'BKLibrary/Assets/*.xcassets'

  s.subspec 'Core' do |sp|
    sp.source_files = 'BKLibrary/Classes/View/**/*'
  end
  s.default_subspec = 'Core'

  # subspecs for all the services.
  s.subspec 'Extension' do |sp|
    sp.dependency 'BKLibrary/Core'
    sp.source_files = 'BKLibrary/Classes/View/**/*'
  end

  s.subspec 'Util' do |sp|
    sp.dependency 'BKLibrary/Core'
    sp.source_files = 'BKLibrary/Classes/Util/**/*'
  end
  
  # s.resource_bundles = {
  #   'BKLibrary' => ['BKLibrary/Assets/*.png']
  # }

end
